module gitlab.com/ponkey364/writescraper

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/bmaupin/go-epub v0.5.3
	github.com/jaffee/commandeer v0.4.0
	github.com/russross/blackfriday/v2 v2.0.1
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092
)
