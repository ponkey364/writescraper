/*
 * Copyright (c) 2020 Ben "Ponkey364" of RPGPN Media
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package main

import (
	"encoding/json"
	"github.com/jaffee/commandeer"
	"gitlab.com/ponkey364/writescraper/scraper"
	"gitlab.com/ponkey364/writescraper/wp"
	"os"
	"strings"
)

func WriteSidecar(export *scraper.GlobalExport, c *Cmd) error {
	for _, chapter := range export.Chapters {
		chapter.Content = nil
	}

	j, err := json.Marshal(export)
	if err != nil {
		panic(err)
	}

	file, err := os.Create(c.Out + ".extra.json")
	if err != nil {
		panic(err)
	}

	_, err = file.Write(j)
	if err != nil {
		panic(err)
	}

	return nil
}

type Cmd struct {
	Platform        string `help:"Which platform to scrape [wattpad]"`
	In              string `help:"The URL to scrape"`
	Format          string `help:"The format to output as [json, epub]"`
	Out             string `help:"The location to write the data to"`
	IncludeComments bool   `help:"Include user's comments in the finished output (or in a sidecar, if format isn't json)"`
	WriteSidecar    bool   `help:"Write a file containing extra data (reads, likes, tags) (true if include-comments is set) (does nothing when format=json)"`
}

func (c *Cmd) Run() error {
	var scraped scraper.Scraped

	switch strings.ToLower(c.Platform) {
	case "wattpad":
		{
			wps := wp.New(c.IncludeComments)

			b, err := wps.Scrape(c.In)
			if err != nil {
				panic(err)
			}

			scraped = b
		}
	}

	export := scraped.Export()

	switch strings.ToLower(c.Format) {
	case "json":
		{
			j, err := json.Marshal(export)
			if err != nil {
				panic(err)
			}

			file, err := os.Create(c.Out)
			if err != nil {
				panic(err)
			}

			_, err = file.Write(j)
			if err != nil {
				panic(err)
			}
		}
	case "epub":
		{
			pub, err := MakeEpub(export)
			if err != nil {
				panic(err)
			}

			err = pub.Write(c.Out)
			if err != nil {
				panic(err)
			}
		}
	}

	if strings.ToLower(c.Format) != "json" && (c.IncludeComments || c.WriteSidecar) {
		err := WriteSidecar(export, c)
		if err != nil {
			panic(err)
		}
	}

	return nil
}

func CmdMain() *Cmd {
	return &Cmd{}
}

func main() {
	err := commandeer.Run(CmdMain())
	if err != nil {
		panic(err)
	}
}
