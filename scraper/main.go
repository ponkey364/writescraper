/*
 * Copyright (c) 2020 Ben "Ponkey364" of RPGPN Media
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package scraper

import "image/color"

type Scraper interface {
	Scrape(URL string) *Scraped
}

type Scraped interface {
	Export() *GlobalExport
}

type GlobalExport struct {
	Title          string                 `json:"title"`
	Tags           []string               `json:"tags"`
	Reads          int                    `json:"reads"`
	Votes          int                    `json:"votes"`
	Description    string                 `json:"description"`
	Chapters       []*GEChapter           `json:"chapters"`
	AdditionalData map[string]interface{} `json:"additional_data"`
}

type GEChapter struct {
	Name           string                 `json:"name"`
	Reads          int                    `json:"reads"`
	Votes          int                    `json:"votes"`
	CreateDate     string                 `json:"create_date"`
	ModifyDate     string                 `json:"modify_date"`
	Comments       []*GEComment           `json:"comments"`
	Content        []*GEContent           `json:"content"`
	AdditionalData map[string]interface{} `json:"additional_data"`
}

type GEComment struct {
	Content        string                 `json:"content"`
	CreateDate     string                 `json:"create_date"`
	ID             string                 `json:"id"`
	Author         string                 `json:"author"`
	Replies        []*GEComment           `json:"replies"`
	AdditionalData map[string]interface{} `json:"additional_data"`
}

type ContentAlign int

const (
	CONTENT_LEFT   ContentAlign = 0
	CONTENT_MIDDLE ContentAlign = 1
	CONTENT_RIGHT  ContentAlign = 2
)

type GEContent struct {
	ID      string
	Content string
	Style   *ContentStyle
}

type ContentStyle struct {
	Alignment ContentAlign
	Colour    *color.RGBA
	ColorStr  *string
}
