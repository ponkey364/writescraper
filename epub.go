/*
 * Copyright (c) 2020 Ben "Ponkey364" of RPGPN Media
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package main

import (
	"fmt"
	"github.com/bmaupin/go-epub"
	"github.com/russross/blackfriday/v2"
	"gitlab.com/ponkey364/writescraper/scraper"
)

func MakeEpub(ex *scraper.GlobalExport) (*epub.Epub, error) {
	pub := epub.NewEpub(ex.Title)
	pub.SetDescription(ex.Description)

	for _, chapter := range ex.Chapters {
		html := MakeHTML(chapter)
		pub.AddSection(html, chapter.Name, "", "")
	}

	return pub, nil
}

func MakeHTML(chap *scraper.GEChapter) string {
	html := "<article>"
	html += fmt.Sprintf("<h2>%s</h2>", chap.Name)

	for _, content := range chap.Content {
		html += "<div style=\""
		switch content.Style.Alignment {
		case scraper.CONTENT_LEFT:
			html += "text-align:left"
		case scraper.CONTENT_MIDDLE:
			html += "text-align:center"
		case scraper.CONTENT_RIGHT:
			html += "text-align:right"
		}
		html += "\">"

		markdown := blackfriday.Run([]byte(content.Content))
		html += string(markdown) + "</div><br/>"
	}

	html += "</article>"

	return html
}
