/*
 * Copyright (c) 2020 Ben "Ponkey364" of RPGPN Media
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package wp

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/ponkey364/writescraper/scraper"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const API_ROOT = "https://wattpad.com/v4"

func RequestToBuffer(URL string, httpClient *http.Client) ([]byte, error) {
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		return nil, err
	}

	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return buf, nil
}

type _WattpadScraper struct {
	httpClient      *http.Client
	IncludeComments bool
}

func (s *ScrapedBook) Export() *scraper.GlobalExport {
	fmt.Println("Exporting...")
	var chapters []*scraper.GEChapter
	for _, chapter := range s.Chapters {
		chaperAdditData := make(map[string]interface{})
		chaperAdditData["photo_url"] = chapter.PhotoURL
		chaperAdditData["video_id"] = chapter.VideoID

		var comments []*scraper.GEComment
		for _, comment := range chapter.Comments {
			commentAdditData := make(map[string]interface{})
			commentAdditData["inline_ref"] = comment.InlineRef
			commentAdditData["is_inline"] = comment.Inline

			var replies []*scraper.GEComment
			for _, reply := range comment.Replies {
				replies = append(replies, &scraper.GEComment{
					Content:        reply.Content,
					CreateDate:     reply.CreateDate,
					ID:             reply.ID,
					Author:         reply.Author,
					Replies:        nil,
					AdditionalData: nil,
				})
			}

			thisComment := &scraper.GEComment{
				Content:        comment.Content,
				CreateDate:     comment.CreateDate,
				ID:             comment.ID,
				Author:         comment.Author,
				Replies:        replies,
				AdditionalData: commentAdditData,
			}
			comments = append(comments, thisComment)
		}

		var content []*scraper.GEContent
		for _, para := range chapter.Content {
			style := ConvertCSS(para.Style)
			colourstring := style["color"]

			var alignment scraper.ContentAlign

			switch style["text-align"] {
			case "center":
				alignment = scraper.CONTENT_MIDDLE
			case "right":
				alignment = scraper.CONTENT_RIGHT
			default:
				alignment = scraper.CONTENT_LEFT
			}

			content = append(content, &scraper.GEContent{
				ID:      para.ID,
				Content: para.Content,
				Style: &scraper.ContentStyle{
					Alignment: alignment,
					ColorStr:  &colourstring,
				},
			})
		}

		thisChapter := &scraper.GEChapter{
			Name:           chapter.Name,
			Reads:          chapter.Reads,
			Votes:          chapter.Votes,
			CreateDate:     chapter.CreateDate,
			ModifyDate:     chapter.ModifyDate,
			Comments:       comments,
			Content:        content,
			AdditionalData: chaperAdditData,
		}
		chapters = append(chapters, thisChapter)
	}

	return &scraper.GlobalExport{
		Title:          s.Title,
		Tags:           s.Tags,
		Reads:          s.Reads,
		Votes:          s.Votes,
		Description:    s.Description,
		Chapters:       chapters,
		AdditionalData: nil,
	}
}

func ConvertCSS(raw string) map[string]string {
	if len(raw) == 0 {
		return nil
	}

	tags := strings.Split(raw, ";")
	if len(tags) == 0 {
		return nil
	}

	out := make(map[string]string)
	for _, v := range tags {
		parts := strings.Split(strings.TrimSpace(v), ":")
		if len(parts) <= 1 {
			continue
		}

		out[strings.TrimSpace(parts[0])] = strings.TrimSpace(parts[1])
	}

	return out
}

func (w *_WattpadScraper) Scrape(URL string) (*ScrapedBook, error) {
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		return nil, err
	}

	res, err := w.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, err
	}

	landingHeader := doc.Find(".story-details-page > .story-header > .story-info")
	landingContainer := doc.Find(".story-details-page > .story-info-container > .left-container")

	headerMeta := landingHeader.Find(".story-stats").Children()

	reads := headerMeta.Get(0).LastChild.FirstChild.FirstChild.FirstChild.Data
	readCount, err := strconv.Atoi(strings.Split(reads[1:], " ")[0])
	if err != nil {
		return nil, err
	}

	votes := headerMeta.Get(1).LastChild.FirstChild.FirstChild.FirstChild.Data
	voteCount, err := strconv.Atoi(strings.Split(votes[1:], " ")[0])
	if err != nil {
		return nil, err
	}

	var tags []string

	landingContainer.Find(".tag-items").Children().Each(func(i int, s *goquery.Selection) {
		tags = append(tags, s.Text())
	})

	var chapters []*Chapter
	landingContainer.Find(".table-of-contents > .story-parts > ul").Children().Each(func(i int, s *goquery.Selection) {
		partId, ok := s.Find("a").Attr("href")
		if !ok {
			fmt.Println(errors.New("key doesn't exist"))
			return
		}
		chapter, err := w.getChapter(strings.Split(partId[1:], "-")[0])
		if err != nil {
			fmt.Println(err)
			return
		}

		chapters = append(chapters, chapter)
	})

	book := &ScrapedBook{
		Title:       strings.TrimSpace(landingHeader.Find("h1").Text()),
		Tags:        tags,
		Reads:       readCount,
		Votes:       voteCount,
		Description: landingContainer.Find(".description").Text(),
		Chapters:    chapters,
	}
	return book, nil
}

func (w *_WattpadScraper) getChapter(chapterID string) (*Chapter, error) {
	fmt.Println(chapterID)

	var comments []*Comment

	if w.IncludeComments {
		commentsOut, err := w.getComments(chapterID)
		if err != nil {
			return nil, err
		}

		comments = commentsOut
	}

	buf, err := RequestToBuffer(fmt.Sprintf("%s/parts/%s", API_ROOT, chapterID), w.httpClient)

	if err != nil {
		return nil, err
	}

	var deepData JSONChapter

	err = json.Unmarshal(buf, &deepData)

	req, err := http.NewRequest("GET", deepData.URL, nil)
	if err != nil {
		return nil, err
	}

	res, err := w.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, err
	}

	var paragraphs []*Paragraph

	textBlock := doc.Find(".panel-reading > pre")

	textBlock.Children().Each(func(i int, s *goquery.Selection) {
		id := s.AttrOr("data-p-id", "")

		style := s.AttrOr("style", "")

		var content string

		for _, node := range s.Nodes {
			content += recTrawlNode(node)
		}

		// sometimes, content will be empty. Ignore it.
		if content == "" {
			return
		}

		paragraphs = append(paragraphs, &Paragraph{
			ID:      id,
			Content: content,
			Style:   style,
		})
	})

	chapter := &Chapter{
		Name:       deepData.Title,
		Reads:      deepData.ReadCount,
		Votes:      deepData.VoteCount,
		CreateDate: deepData.CreateDate,
		ModifyDate: deepData.ModifyDate,
		VideoID:    deepData.VideoID,
		PhotoURL:   deepData.PhotoURL,
		Comments:   comments,
		Content:    paragraphs,
	}
	return chapter, nil
}

func recTrawlNode(node *html.Node) string {
	var data string
	var end string

	switch node.Type {
	case html.ElementNode:
		{
			switch node.DataAtom {
			case atom.B:
				{
					data += "**"
					end = "**"
				}
			case atom.I, atom.Em:
				{
					data += "*"
					end = "*"
				}
			case atom.U:
				{
					data += "__"
					end = "__"
				}
			case atom.Br:
				{
					data += "\n\n"
				}
			}
		}
	case html.TextNode:
		{
			trimmed := strings.TrimSpace(node.Data)
			data += escapeMD(trimmed)
		}
	}

	if node.FirstChild != nil {
		if node.FirstChild.DataAtom != atom.P {
			data += recTrawlNode(node.FirstChild)
		}
	}

	data += end

	// now go sideways, like the cha-cha-slide
	if node.NextSibling != nil {
		if node.NextSibling.DataAtom != atom.P {
			data += recTrawlNode(node.NextSibling)
		}
	}

	return data
}

var markdownCharsRE = regexp.MustCompile("([\\[*_`~])")

func escapeMD(in string) string {
	repl := markdownCharsRE.ReplaceAll([]byte(in), []byte("\\$1"))
	return string(repl)
}

func (w *_WattpadScraper) getComments(partID string) ([]*Comment, error) {
	buf, err := RequestToBuffer(fmt.Sprintf("%s/parts/%s/comments", API_ROOT, partID), w.httpClient)
	if err != nil {
		return nil, err
	}

	var jc struct {
		Comments []*JSONComment `json:"comments"`
	}

	err = json.Unmarshal(buf, &jc)

	if err != nil {
		return nil, err
	}

	var comments []*Comment

	ch := make(chan *Comment)

	for _, value := range jc.Comments {
		go func(channel chan *Comment, v *JSONComment) {
			var (
				replies   []*ReplyComment = nil
				inline                    = false
				inlineRef *string         = nil
			)

			if v.NumReplies > 0 {
				buf, err := RequestToBuffer(fmt.Sprintf("%s/comments/%s/replies", API_ROOT, v.ID), w.httpClient)
				if err != nil {
					fmt.Println(err)
					return
				}

				var replyData struct {
					Replies []*JSONComment
				}

				err = json.Unmarshal(buf, &replyData)
				if err != nil {
					fmt.Println(err)
					return
				}

				for _, reply := range replyData.Replies {
					thisReply := &ReplyComment{
						Content:    reply.Body,
						CreateDate: reply.CreateDate,
						ID:         reply.ID,
						Author:     reply.Author.Name,
					}
					replies = append(replies, thisReply)
				}
			}

			if v.ParagraphID != nil {
				inline = true
				inlineRef = v.ParagraphID
			}

			thisComment := &Comment{
				Content:    v.Body,
				CreateDate: v.CreateDate,
				Inline:     inline,
				InlineRef:  inlineRef,
				ID:         v.ID,
				Author:     v.Author.Name,
				Replies:    replies,
			}

			channel <- thisComment
		}(ch, value)
	}

	for range jc.Comments {
		thisComment := <-ch
		comments = append(comments, thisComment)
	}

	return comments, nil
}

func New(includeComments bool) *_WattpadScraper {
	return &_WattpadScraper{
		IncludeComments: includeComments,
		httpClient: &http.Client{
			Timeout: time.Second * 10,
		},
	}
}
