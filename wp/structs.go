/*
 * Copyright (c) 2020 Ben "Ponkey364" of RPGPN Media
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package wp

type ScrapedBook struct {
	Title       string
	Tags        []string
	Reads       int
	Votes       int
	Description string
	Chapters    []*Chapter
}

type Chapter struct {
	Name       string
	Reads      int
	Votes      int
	CreateDate string
	ModifyDate string
	VideoID    string
	PhotoURL   string
	Comments   []*Comment
	Content    []*Paragraph
}

type Paragraph struct {
	ID      string
	Content string
	Style   string // Just for now, this'll be better eventually
}

type Comment struct {
	Content    string
	CreateDate string
	Inline     bool
	InlineRef  *string
	ID         string
	Author     string
	Replies    []*ReplyComment
}

type ReplyComment struct {
	Content    string
	CreateDate string
	ID         string
	Author     string
}

/*
*********************
*	JSON Structs	*
*********************
 */

type JSONComment struct {
	Body        string  `json:"body"`
	CreateDate  string  `json:"createDate"`
	ID          string  `json:"id"`
	ParagraphID *string `json:"paragraphId, omitempty"`
	Author      struct {
		Name string `json:"name"`
	} `json:"author"`
	NumReplies int `json:"numReplies"`
}

type JSONChapter struct {
	ID           string `json:"id"`
	Title        string `json:"title"`
	ModifyDate   string `json:"modifyDate"`
	CreateDate   string `json:"createDate"`
	VideoID      string `json:"videoId"`
	PhotoURL     string `json:"photoUrl"`
	CommentCount int    `json:"commentCount"`
	VoteCount    int    `json:"voteCount"`
	ReadCount    int    `json:"readCount"`
	URL          string `json:"url"`
}
