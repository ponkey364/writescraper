# Writescrape

A tool for scraping popular writing sites

## Supported

- [x] Wattpad
- [ ] AO3
- [ ] Fanfiction.net

## Usage

```
Usage of writescrape:
  -format string
        The format to output as  
        [json, epub]
  -in string
        The URL to scrape
  -include-comments
        Include user's comments in the finished output (or in a sidecar, if format isn't json)
  -out string
        The location to write the data to
  -platform string
        Which platform to scrape  
        [wattpad]
    -write-sidecar
        Write a file containing extra data (reads, likes, tags) (true if include-comments is set) (does nothing when format=json)
```

## Site-specific info

### Wattpad
Inherited from my old wpscrape project, just ported to a more sensible language
